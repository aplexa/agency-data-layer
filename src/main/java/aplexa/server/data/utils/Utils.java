package aplexa.server.data.utils;

import java.util.Date;

public class Utils {

    private Utils () throws IllegalAccessException {
        throw new IllegalAccessException("Utility class");
    }

    public static String createSerial(String customerId, Long consecutive) {
        return customerId +
                consecutive +
                new Date().getTime();
    }
}
